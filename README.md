# Associate Support Engineer Take Home Assessment

## Question 1: Write a Ruby or Bash script that will print usernames of all users on a Linux system together with their home directories. 

Answer:

#!/bin/bash <br>
while IFS=: read -r username _ _ _ homedir _; do<br> 
   &nbsp;&nbsp; echo "`$`username:`$`homedir"<br>
done < /placeholder/file 

Line 2 was referenced with chatgpt. I was unsure on the format for the read function in the while loop. 

## Question 2: Study the Git commit graph shown below. What sequence of Git commands could have resulted in this commit graph?

Answer:  
1. An edit is made.  
2. git commit e137e9b.  
3. An edit is made.  
4. git commit 9359569.  
5. create a branch with an edit: git checkout -b feature-branch (889a8ac).  
6. return to master branch: git checkout master.  
7. An edit is made.  
8. git commit 73e170d.  
9. Merge feature branch with main branch: git merge feture-branch (e047b43).  
10. An edit is made.  
11. git commit 8c574a3.  


## Question 3: Write a brief blog post for GitLab that explains what Git is and what it can do for you.

Answer:

Git is a free and open sourced distributed version control system that handles small to very large projects with speed and efficiency. Its a system for developers to manage their codebases, collaborate and coordinate work with teams, and track changes in their codebase over time as well as maintaining a complete history of all modifications.
Git has many key features such as Distributed Version Control, History Tracking, the Staging Area, and many more. The distributed version control feature is where each developer is allowed a local copy of the entire project repo. This gives control to many members which enables offline work, encourages collaboration, and checks for redundancy. Gits history tracking is a comprehensive history of changes made to the codebase. It allows us to know who made the changes, when they were made, and why. This ensures accountability, enables easy reverts to previous states, and benefits in debugging and troubleshooting. Git features a staging area, where you select the changes you want before committing them to the repository. This ensures precise commits where only relevant changes are included.

Git is a powerful tool for developers and teams to collaborate effectively, track changes, and build high-quality software with confidence. Developers gain smooth workflows, enhanced productivity, and the ability to be creative. 


## Question 4: Tell me about a recent issue you debugged or a problem you solved. How did you go about debugging it? What tools did you use? What was the outcome?

Answer: 

In a group project, my responsibility was to develop the front end while others focused on the back end. As our demo deadline approached, it became evident that a significant portion of the back end was incomplete. Despite my efforts to communicate the urgency of completing it, my team members were uncooperative. To ensure we had something to present during the demo, I resorted to hard coding the back end, allowing us to showcase the intended functionality of the project.

While working on the front end, I encountered a critical issue where adding certain components to the main page would cause it to crash inexplicably. Despite consulting with the teacher, teacher assistants, and classmates, we were unable to pinpoint the root cause. Eventually, I turned to online resources and discovered an IDE called Replit, which allowed for quick testing without the overhead of loading the entire web application repeatedly. Through diligent investigation on Replit, I eventually identified a specific line of code responsible for the crashes. Although I successfully removed the problematic code and got the page running on Replit, I encountered difficulty implementing the component on the page within my source code in Visual Studio Code.

#### References:<br>
[Markdown Formatting](https://www.markdownguide.org/basic-syntax/)<br>
[Gitlab handbook](https://handbook.gitlab.com/)<br>
[Gitlab](https://gitlab.com/)<br>
Gitlab duo chat beta<br>
[Git Commands Referenced](https://www.google.com/search?q=git+commands&oq=git+commands&gs_lcrp=EgZjaHJvbWUqDAgAEAAYChixAxiABDIMCAAQABgKGLEDGIAEMgkIARAAGAoYgAQyCQgCEAAYChiABDIJCAMQABgKGIAEMgkIBBAAGAoYgAQyCQgFEAAYChiABDIGCAYQRRg8MgYIBxBFGDzSAQg1ODQ0ajBqN6gCALACAA&sourceid=chrome&ie=UTF-8)<br>
[Git](https://www.simplilearn.com/tutorials/git-tutorial/what-is-git)<br>
